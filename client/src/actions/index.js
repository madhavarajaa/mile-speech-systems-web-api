import axios from 'axios';
import History from '../history.js';
import {
    AUTH_USER,
    UNAUTH_USER,
    AUTH_ERROR,
    FETCH_FEATURE,
    GET_LANGUAGES
} from './types';

const ROOT_URL = 'http://localhost:3090/api';

export const signinUser = ({ userID, password }) => {
    return (dispatch) => {
        // submit userID/password to the server
        axios.post(`${ROOT_URL}/signin`, { userID, password })
            .then(response => {

                // if request is good...
                // - update state to indicate user is authenticated
                dispatch({
                  type: AUTH_USER
                });

                // - save the jwt token
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));

                // - redirect to the route '/feature'
                History.push('/');

            }).catch(() => {
                // if request is bad...
                // - show an error to the user
                dispatch(authError('Bad Login Info'));
            });
    };
};

export const signupUser = ({ userID, password }) => {
    return (dispatch) => {
        // submit userID/password to the server
        axios.post(`${ROOT_URL}/signup`, { userID, password })
            .then(response => {
                dispatch({ type: AUTH_USER });
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('userID', userID);
                History.push('/feature');
            })
            .catch(err => {
                dispatch(authError(err.response.data.error));
            });
    };
};

export const authError = (error) => {
    return {
        type: AUTH_ERROR,
        payload: error
    };
};

export const signoutUser = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('user');
    alert('You have been sucessfully signed out')
    return { type: UNAUTH_USER };
};

export const fetchLanguages = () => {
    return (dispatch) => {
        axios.get(`${ROOT_URL}/getLanguages`, {
            headers: { authorization: localStorage.getItem('token') }
        })
        .then(response =>{
            dispatch({
                type: GET_LANGUAGES,
                payload: response.data
             });
        });
    };
};

export const speakerExists = ({ userID, languageID, speakerID, speakerLangID }) => {
    return (dispatch) => {
        // submit userID/password to the server
        axios.post(`${ROOT_URL}/speakerExists`, { userID, languageID, speakerID, speakerLangID  })
            .then(response => {
              if(!response.data.exists){
                History.push({
                              pathname:`/dataCollection/${languageID}`,
                              state:{languageID:languageID, speakerID:speakerID}
                            })
              }else{
                var data =response.data.data;
                data.selectedLang = languageID;
                History.push({
                  pathname:`/dataCollection/${languageID}/${speakerID}`,
                  state:data
                })
              }

            })
    };
};

export const registerSpeaker = (data) => {
    return (dispatch) => {
        axios.post(`${ROOT_URL}/addSpeaker`,data)
            .then(response => {
              console.log(response.data);
              // make a call and get response then, add other things as total audio recoreded.
              History.push({
                pathname:`/dataCollection/${response.data.languageID}/${response.data.speakerID}`,
                state:response.data
              })
            })
    };
};

export const getUtterances = (data) => {
    axios.post(`${ROOT_URL}/getUtterances`,data)
          .then(response => {
            console.log(response.data);
            // make a call and get response then, add other things as total audio recoreded.
            return response.data
      })
}


export const fetchFeature = () => {
    return (dispatch) => {
        axios.get(`${ROOT_URL}`, {
            headers: { authorization: localStorage.getItem('token') }
        })
        .then(response =>{
            dispatch({
                type: FETCH_FEATURE,
                payload: response.data
             });
        });
    };
};
