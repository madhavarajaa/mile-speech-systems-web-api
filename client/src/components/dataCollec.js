import React, { Component } from 'react';
import { connect } from 'react-redux';
import AudioRecorder from './react-audio-recorder/dist/AudioRecorder';
import  InputBox from './inputBox';
import * as actions from '../actions';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Blob from 'blob';

class DataCollec extends Component{
  constructor(props){
    super(props);
    this.state={
      speakerDetails:this.props.location.state,
      utterances:'',
      currentSelectedUtteranceIndex:0,
      currentRecording:{duration:0,audioData:null},
      inputBoxText:'This is a Sample Text'
    }
    this.audioRef = React.createRef();
    this.onAudioChangeHandler = this.onAudioChangeHandler.bind(this);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onInputBoxChange = this.onInputBoxChange.bind(this);
    this.nextUtterHandler = this.nextUtterHandler.bind(this);
    this.prevUtterHandle = this.prevUtterHandle.bind(this);
    this.findPrevUnrecord = this.findPrevUnrecord.bind(this);
    this.findNextUnrecord = this.findNextUnrecord.bind(this);
    this.selectDifferentLanguage = this.selectDifferentLanguage.bind(this);
    this.makeAnAudio = this.makeAnAudio.bind(this);
  }

  componentDidMount(){
    var user = JSON.parse(localStorage.getItem('user'));
    var ROOT_URL='http://localhost:3090/api'
    var data={
      "speakerID":parseInt(this.state.speakerDetails.speakerID),
      "languageID":parseInt(this.state.speakerDetails.selectedLang),
      "userID":user.userID
    }
    axios.post(`${ROOT_URL}/getUtterances`,data)
      .then(response =>{
          var inputBoxText=response.data[0]['originalText'];
          this.setState({utterances:response.data,inputBoxText:inputBoxText});
      });
  }

  onAudioChangeHandler(data){
    console.log(data);
    if(data){
      this.setState({currentRecording:data})
    }else{
      this.setState({currentRecording:{}})
    }
  }

  onInputBoxChange(newText){
    this.setState({inputBoxText:newText})
  }

  nextUtterHandler(){
    var utterIndex = this.state.currentSelectedUtteranceIndex;
    if( utterIndex == this.state.utterances.length-1 ){
      utterIndex=-1;
    }
    var nextIndex=utterIndex+1;
    console.log('nextIndex:'+nextIndex)
    this.setState({
      currentSelectedUtteranceIndex:nextIndex,
      inputBoxText:this.state.utterances[nextIndex]['originalText']
    })
  }

  prevUtterHandle(){
    var utterIndex = this.state.currentSelectedUtteranceIndex;
    if( utterIndex == 0 ){
      utterIndex=this.state.utterances.length;
    }
    var prevIndex=utterIndex-1;
    this.setState({
      currentSelectedUtteranceIndex:prevIndex,
      inputBoxText:this.state.utterances[prevIndex]['originalText']
    })
  }

  findPrevUnrecord(){
    var utterIndex = this.state.currentSelectedUtteranceIndex;
    if( utterIndex == 0 ){
      utterIndex=this.state.utterances.length;
    }
    var prevUtter = utterIndex-1;
    var foundRecord = this.state.utterances[prevUtter]['isRecorded'];
    while(foundRecord){
      if(prevUtter == this.state.currentSelectedUtteranceIndex){
        foundRecord=false;
      }else if( prevUtter == 0){
        prevUtter = this.state.utterances.length;
      }
      prevUtter = prevUtter -1;
      foundRecord = this.state.utterances[prevUtter]['isRecorded'];
    }
    this.setState({
      currentSelectedUtteranceIndex:prevUtter,
      inputBoxText:this.state.utterances[prevUtter]['originalText']
    })
  }

  findNextUnrecord(){
    var utterIndex = this.state.currentSelectedUtteranceIndex;
    if( utterIndex == this.state.utterances.length-1 ){
      utterIndex=-1;
    }
    var nextUtter = utterIndex+1;
    var foundRecord = this.state.utterances[nextUtter]['isRecorded'];
    while(foundRecord){
      if(nextUtter == this.state.currentSelectedUtteranceIndex){
        foundRecord=false;
      }else if( nextUtter == this.state.utterances.length-1){
        nextUtter = -1;
      }
      nextUtter = nextUtter +1;
      foundRecord = this.state.utterances[nextUtter]['isRecorded'];
    }
    this.setState({
      currentSelectedUtteranceIndex:nextUtter,
      inputBoxText:this.state.utterances[nextUtter]['originalText']
    })
  }

  onSubmitHandler(){
    //console.log(this.state);
    // Initiate one request here;
  }

  selectDifferentLanguage(){
    this.props.history.push('/dataCollection')
  }

  makeAnAudio(){
    var binaryData = [];
    binaryData.push(this.state.currentRecording.audioData);
    console.log(binaryData);
    return
  }

  render(){
    console.log(this.state);
    const recordLabel = <img src="http://www.iitg.ac.in/priyankoo/audio/img/recording.gif" width="20px"/>
    const recordingLabel= <img src="http://www.iitg.ac.in/priyankoo/audio/img/stop_recording.gif" width="20px" />
    return(
        <div className='fluid-container'>
          <div className="row">
            <div className="col-md-9 col-sm-8">
              <InputBox text={this.state.inputBoxText} textOnChange={this.onInputBoxChange}/>
            </div>
            <div className="col-md-3">
              <div className="utter-info-card">
                <div className="info-container  card">
                  <div className="card-title">
                    <h5>Details </h5>
                  </div>
                  <div className="card-body">
                    <span className="info-detail-label">Total Recorded utterance</span><br />
                    <span className="info-detail-Value">{this.state.speakerDetails.recordedUtterance}</span><br />
                    <span className="info-detail-label">Total Recorded time</span><br />
                    <span className="info-detail-value">{this.state.speakerDetails.recordedTime}</span><br />
                    <span className="info-detail-label">Total Utterances</span><br />
                    <span className="info-detail-value">{this.state.utterances.length}</span><br />
                    <span className="info-detail-label">Current Utternace</span><br />
                    <span className="info-detail-value">{this.state.currentSelectedUtteranceIndex}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-1"></div>
            <div className="col-md-10">
              <div className="row">
              </div>
              <div className="row">
                <AudioRecorder
                  onChange={this.onAudioChangeHandler}
                  downloadable={false}
                  initialAudio = {this.state.currentRecording.audioData}
                  ref={this.audioRef}
                  removeLabel	={recordLabel}
                  recordLabel={recordLabel}
                  recordingLabel={recordingLabel}
                  />
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="row audo-play-menu-container">
                    <button className='btn' onClick={this.prevUtterHandle} >Prev Transciption</button>
                    <button className='btn' onClick={this.nextUtterHandler}>Next Transciption</button><br/>
                  </div>
                  <div className="row audo-play-menu-container">
                    <button className='btn' onClick={this.findPrevUnrecord}>Prev Unrecorded</button>
                    <button className='btn' onClick={this.findNextUnrecord}>Next Unrecorded</button><br/>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-6">
                      <button
                      className='big-buttons-controller-data-record'
                      onClick={this.selectDifferentLanguage}>
                        Select a different language
                      </button>
                    </div>
                    <div className="col-md-6">
                      <button
                      className="big-buttons-controller-data-record"
                      onClick={this.onSubmitHandler}>
                        Save and Submit recording
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-1"></div>
          </div>
        </div>
    )
  }
}

export default connect(null, actions)(DataCollec);
