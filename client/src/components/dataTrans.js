import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import LanguageSelect from './languageSelect';
import { Link } from 'react-router-dom';
import  InputBox from './inputBox'



class DataCollec extends Component {

    constructor(props){
      super(props);
      this.state = {
        value: 'Please write an essay about your favorite DOM element.'
      };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
      this.setState({value: event.target.value});
    }

    handleSubmit(event) {
      console.log('An essay was submitted: ' + this.state.value);
      event.preventDefault();
    }


    render() {
        return (
            <div>
                {!(this.props.match.params.lang) ? <LanguageSelect /> :
                  <div>
                    <form onSubmit={this.handleSubmit}>
                      <div>
                        <InputBox />
                      </div>
                      <audio controls>
                        <source src="https://freewavesamples.com/files/Yamaha-V50-Metalimba-C2.wav" type="audio/mpeg" />
                      Your browser does not support the audio element.
                      </audio>
                      <div className="audo-play-menu-container">
                        <button className='btn'>Prev Transciption</button>
                        <button className='btn'>Next Transciption</button>
                        <button className='btn'>
                          <Link to={`/dataTrans`}> Go Back </Link>
                        </button>
                        <button className='btn' type="submit" value="Submit"> Submit </button>
                       </div>
                     </form>
                    </div>
                 }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { features: state.features.homePageFeatures }
}


export default connect(mapStateToProps, actions)(DataCollec);
