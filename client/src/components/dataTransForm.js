import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Link } from 'react-router-dom';

var features=['a','b','c']

class LanguageSelect extends PureComponent {

    constructor(props){
      super(props);
      this.state = {
        lang:'',
        type:''
      }
      this.handleLangSelectChange = this.handleLangSelectChange.bind(this);
      this.handleTypeSelectChange = this.handleTypeSelectChange.bind(this);
    }

    componentWillMount() {
        this.props.fetchFeature();
    }

    renderFeature() {
        return features.map(feature => {
            return <option key={feature+'dssd'} value={feature}>{feature}</option>;
        })
    }

    handleTypeSelectChange(e){
        this.setState({type:e.target.value})
    }

    handleLangSelectChange(e){
        this.setState({lang:e.target.value})
    }

    render() {
        var pathname=window.location.pathname;

        return (
            <div className="language-container">
              <div className="form-group">
                <h6>Select a language </h6>
                <select className="form-control" onChange={this.handleLangSelectChange}>
                  <option key='whyAreYouHere?GoBackToBrowser' value="">Languages available </option>
                  {this.renderFeature()}
                </select>
              </div>
              <div className="form-group">
                <h6> Select Transcription Type </h6>
                <select className="form-control" onChange={this.handleTypeSelectChange}>
                  <option value="qa1">QA1 </option>
                  <option value="qa2">QA2</option>
                  <option value="air1">AIR 1</option>
                  <option value="air2">AIR 2</option>
                </select>
              </div>
              {(this.state.lang && this.state.type) && <button  className="btn btn-outline-primary">
                <Link className="nav-link" to={`${pathname}/${this.state.lang}/${this.state.type}`}> Continue </Link>
              </button>}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { features: state.features.homePageFeatures }
}

export default connect(mapStateToProps, actions)(LanguageSelect);
