import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as actions from './../actions';


class Header extends PureComponent {

  constructor(props){
    super(props);
    this.signoutHandler=this.signoutHandler.bind(this);
  }


    signoutHandler(){
      this.props.signoutUser();
    }

    renderLinks() {
        if (this.props.authenticated) {
            return (
                <li className="nav-item" onClick={this.signoutHandler}>
                    <Link className="nav-link" to="/">Sign out</Link>
                </li>
            );
        } else {
            return [
                <li className="nav-item" key="signin">
                    <Link className="nav-link" to="/signin">Sign in</Link>
                </li>
            ];
        }
    }

    render() {
        return (
            <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
              <div className="navbar-header">
                <Link to="/" className="navbar-brand">
                  <div className="row">
                    <div className="col-md-6">
                      <img src={require("../images/logo.png")} alt={''} />
                    </div>
                    <div className="col-md-6 navbar-text">
                      <div className="row">
                        <span className="text-white font-obroto"><b>MILE Lab</b></span>
                      </div>
                      <div className="row">
                        <span id="mile-nav-text-small">
                          Medical Intelligence and <br/> Language Engineering lab
                        </span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="navbar-nav mx-auto project-title-nav font-obroto">
                MILE SPEECH SYSTEMS
              </div>
              <ul className="navbar-nav ml-md-auto nav-left-container ">
                <li className="nav-item">
                  <Link className="nav-link" to="/faq">FAQ</Link>
                </li>
                {this.renderLinks()}
              </ul>
            </nav>
        );
    }
}

const mapStateToProps = (state) => {
    return { authenticated: state.auth.authenticated }
};

export default connect(mapStateToProps,actions)(Header);
