import React, { Component, Fragment } from 'react';

export default class InputBox extends Component{
  constructor(props){
    super(props);
    this.state={
      inputFont:24,
      textBoxValue:''
    }
    this.increaseFont = this.increaseFont.bind(this);
    this.decreaseFont = this.decreaseFont.bind(this);
    this.handleTextBoxChange = this.handleTextBoxChange.bind(this);
  }

  componentDidMount(){
    if(this.props.text){
      this.setState({textBoxValue:this.props.text})
    }
  }

  decreaseFont(){
    var currentFont = this.state.inputFont;
    this.setState({inputFont: currentFont -1});
  }

  increaseFont(){
    var currentFont = this.state.inputFont;
    this.setState({inputFont: currentFont + 1});
  }

  handleTextBoxChange(e){
    this.props.textOnChange(e.target.value);
    this.setState({textBoxValue:e.target.value});
  }

  render(){
    var customStyle  = {
                          fontSize : this.state.inputFont + 'px',
                          height: 300 + 'px',
                        }
    return(
      <Fragment>
        <div className='row'>
          <div className="col-md-1 col-sm-1">
            <div className='row inputBox-button'>
              <button id="increase-font" onClick={this.increaseFont}> + </button>
            </div>
            <div className='row inputBox-button'>
              <button id="decrease-font" onClick={this.decreaseFont}> - </button>
            </div>
          </div>
          <div className="col-md-11 col-md-11">
            <textarea
              className="form-control"
              style={customStyle}
              rows="5"
              value={this.props.text}
              onChange={this.handleTextBoxChange} >
            </textarea>
          </div>
        </div>
      </Fragment>
    )
  }
}
