import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
class LanguageSelect extends PureComponent {

    constructor(props){
      super(props);
      this.state = {
        langID:'',
        user:{},
        speakerID:''
      }
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleLanguageChange= this.handleLanguageChange.bind(this);
      this.handleSpeakerChange= this.handleSpeakerChange.bind(this);
    }

    componentWillMount() {
      this.props.fetchLanguages();
      var user= JSON.parse(localStorage.getItem('user'));
      if(!!user){
        this.setState({user:user,langID:user.languageID})
      }else{
        this.props.history.push('/');
      }
    }

    handleSubmit(e){
      e.preventDefault();
      // to={`${pathname}/${this.state.langID}`}
      if( this.state.speakerID >= this.state.user.speakerStart && this.state.speakerID <= this.state.user.speakerEnd){
        //Do check it with the main language and while fetching use the ne chosen by user
        this.props.speakerExists({
          userID:this.state.user.userID,
          speakerID:parseInt(this.state.speakerID,10),
          languageID:parseInt(this.state.langID,10),
          speakerLangID:parseInt(this.state.user.languageID,10)
        });
      }else{
          alert(`Invalid speaker ID. Please choose between ${this.state.user.speakerStart} to ${this.state.user.speakerEnd}`)
      }
    }

    handleLanguageChange(e){
      this.setState({langID:parseInt(e.target.value,10)});
    }

    handleSpeakerChange(e){
      var val= e.target.value;
      this.setState({speakerID:val})
    }

    findLanguageName(id){
      if(this.props.languages){
        var result = this.props.languages.filter(lang => {
          return lang.langID === id
        })
        return result[0]['name'];
      }
      return ''
    }

    render() {
        const user = this.state.user;
        var langName=""
        if(user){
          langName = this.findLanguageName(user.languageID);
        }
        return (
          <div className="card">
            <div className="card-body">
              <div className="language-container">
                <form>
                  <div className="form-group" onChange={this.handleLanguageChange}>
                    <h6> Please select a language to continue </h6>
                    { langName && <label className="radio-inline"><input type="radio" name="lang" value={user.languageID} defaultChecked/>{langName}</label>}
                    <label className="radio-inline"><input type="radio" name="lang" value={12}/>English</label>
                  </div>
                  <div className="form-group" onChange={this.handleSpeakerChange}>
                    <h6>Enter SpeakerID:</h6>
                    <input type="number" className="form-control" id="email" />
                  </div>
                  <button type="Submit" className="btn btn-language-select" onClick={this.handleSubmit}>
                    Submit
                  </button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { languages: state.language.languages }
}

export default connect(mapStateToProps, actions)(LanguageSelect);
