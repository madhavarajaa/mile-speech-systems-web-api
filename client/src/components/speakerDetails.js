import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Link } from 'react-router-dom';


class SpeakerDetails extends PureComponent {

    constructor(props){
      super(props);
      this.state={
        id:'',
        name:'',
        region:'',
        age:'',
        contact:'',
        sex:'',
        tncChecked:false
      }
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handleSpeakerIDChange = this.handleSpeakerIDChange.bind(this)
      this.handleNameChange = this.handleNameChange.bind(this)
      this.handleAgeChange = this.handleAgeChange.bind(this)
      this.handleRegionChange = this.handleRegionChange.bind(this)
      this.handleContactChange = this.handleContactChange.bind(this)
      this.handleSexChange = this.handleSexChange.bind(this)
      this.handletncChange = this.handletncChange.bind(this)
    }

    handleSubmit(event) {
      event.preventDefault();
      var user = JSON.parse(localStorage.getItem('user'))
      if(this.state.tncChecked){
        var speakerDetails = {
          speakerID: parseInt(this.state.id,10),
          name:this.state.name,
          age:parseInt(this.state.age,10),
          gender:this.state.sex,
          contact:this.state.contact,
          dialect:this.state.region,
          languageID:parseInt(this.props.match.params.lang,10),
          userID:user.userID
        }
        // make a call and get response then, add other things as total audio recoreded.
        this.props.registerSpeaker(speakerDetails)
      }else{
        alert('Please check terms and conditions fields')
      }
    }

    componentWillMount() {
      this.setState({id:this.props.location.state.speakerID})
    }

    handleSpeakerIDChange(e){
      this.setState({id:e.target.value});
    }

    handleNameChange(e){
      this.setState({name:e.target.value});
    }

    handleRegionChange(e){
      this.setState({region:e.target.value});
    }

    handleAgeChange(e){
      this.setState({age:e.target.value});
    }

    handleContactChange(e){
      this.setState({contact:e.target.value});
    }

    handleSexChange(e){
      this.setState({sex:e.target.value})
    }
    handletncChange(e){
      this.setState({tncChecked:e.target.checked})
    }
    render() {
        return (
          <div className="">
                <Fragment>
                <div className="card speaker-details-container">
                  <div className="card-body">
                    <form onSubmit={this.handleSubmit}>
                      <div className="form-group">
                        <label>Speaker Id</label>
                        <input type="text"
                          className="form-control"
                          value={this.state.id}
                          onChange={this.handleSpeakerIDChange}
                          required />
                      </div>
                      <div className="form-group">
                        <label>Name</label>
                        <input type="text"
                          className="form-control"
                          value={this.state.name}
                          onChange={this.handleNameChange}
                          required />
                      </div>
                      <div className="form-group">
                        <label>Dialect Region</label>
                        <input type="text"
                          className="form-control"
                          value={this.state.region}
                          onChange={this.handleRegionChange}
                          required />
                      </div>
                      <div className="form-group">
                        <label>Age</label>
                        <input type="number"
                          className="form-control"
                          value={this.state.age}
                          onChange={this.handleAgeChange}
                          required />
                      </div>
                      <div className="form-group">
                        <label>Sex</label>
                        <select type="text"
                          className="form-control"
                          value={this.state.sex}
                          onChange={this.handleSexChange}>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                          <option value="others">Others</option>
                        </select>
                      </div>
                      <div className="form-group">
                        <label>Contact</label>
                        <input type="text"
                          className="form-control"
                          value={this.state.contact}
                          onChange={this.handleContactChange}/>
                      </div>
                      <div className="form-group">
                        <input
                          type="checkbox"
                          onClick={this.handletncChange}/>
                        <span data-toggle="modal" data-target="#myModal" id="SpeakertermsAndContions"> I accept all Terms and Conditions</span>
                      </div>
                      <div className="form-btn-grp">
                        <button>
                         <Link to={`/dataCollection`}> Go Back </Link>
                       </button>
                        <button type="submit"> Continue </button>
                      </div>
                  </form>
                </div>
              </div>
              <div className="modal fade" id="myModal" role="dialog">
                <div className="modal-dialog">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h4 className="modal-title">Terms and Conditions</h4>
                    </div>
                    <div className="modal-body">
                    <p>1) The speech data collected from me (henceforth refered to as "speaker") can be used for academic research and commercial purposes.</p>
                    <p>2) The speaker holds no ownership over this speech data or over any other softwares developed using this data.</p>
                    <p>3) The speaker voluntarily agrees his/her voice samples to be recorded by the volunteer.</p>
                    <p>4) We will uphold the confidential information discolosed to us (in terms of photograph, name, age, gender, contact details, dialect region, etc.,). This data will be used only for our internal data analytics purposes.</p>
                    </div>
                    <div className="modal-footer">
                      <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
              </Fragment>
          </div>
        );
      }
}

const mapStateToProps = (state) => {
    return { features: state.features.homePageFeatures }
}

export default connect(mapStateToProps, actions)(SpeakerDetails);
