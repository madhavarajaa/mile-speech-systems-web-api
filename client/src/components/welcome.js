import React from 'react';
import { Link } from 'react-router-dom';

export default () => {
  return(
    <div className="menu-container">
      <div className="btn btn-outline-primary menu-main-btn">
        <Link className="nav-link" to="/asr">
         Speech Recognition </Link>
      </div>
      <div className="btn btn-outline-primary menu-main-btn">
        <Link className="nav-link" to="/dataCollection"> Speech Data Collection </Link>
      </div>
      <div className="btn btn-outline-primary menu-main-btn">
        <Link className="nav-link" to="/dataTrans"> Speech Transcription </Link>
      </div>
    </div>
  )
}
