import {
    GET_LANGUAGES
} from '../actions/types';

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case GET_LANGUAGES:
            return { ...state, languages: action.payload}
        default:
            return state;
    }
};
