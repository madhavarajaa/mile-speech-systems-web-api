import React from 'react';
import { Route } from 'react-router-dom';
import App from '../components/app';
import RequireAuth from '../components/auth/require_auth';
import Signin from '../components/auth/signin';
import Signout from '../components/auth/signout';
import Faq from '../components/faq';
import Welcome from '../components/welcome';
import Asr from '../components/asr';
import SpeakerDetails from '../components/speakerDetails';
import DataTransForm from '../components/dataTransForm';
import DataCollec from '../components/dataCollec';
import DataTrans from '../components/dataTrans'
import LanguageSelect from '../components/languageSelect'




const Routes = () => {
    return (
        <App>
            <Route exact path="/" component={Welcome} />
            <Route exact path="/signin" component={Signin} />
            <Route exact path="/signout" component={Signout} />
            <Route exact path="/asr" component={Asr} />
            <Route exact path="/dataTrans" component={RequireAuth(DataTransForm)} />
            <Route exact path="/dataTrans/:lang/:type" component={RequireAuth(DataTrans)} />
            <Route exact path="/dataCollection" component={RequireAuth(LanguageSelect)} />
            <Route exact path="/dataCollection/:lang" component={RequireAuth(SpeakerDetails)} />
            <Route exact path="/dataCollection/:lang/:speakerID" component={RequireAuth(DataCollec)} />
            <Route exact path="/faq" component={Faq} />
        </App>
    );
};

export default Routes;
