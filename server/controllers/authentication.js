const jwt = require('jwt-simple');
const User = require('../models/user');
const config = require('../config');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
  res.send({ token: tokenForUser(req.user), user:req.user });
}

exports.signup = function(req, res, next) {
  const userID = req.body.userID;
  const password = req.body.password;

  if (!userID || !password) {
    return res.status(422).send({ error: 'You must provide userID and password'});
  }

  // See if a user with the given userID exists
  User.findOne({ userID: userID }, function(err, existingUser) {
    if (err) { return next(err); }

    // If a user with userID does exist, return an error
    if (existingUser) {
      return res.status(422).send({ error: 'userID is in use' });
    }

    // If a user with userID does NOT exist, create and save user record
    const user = new User({
      userID: userID,
      password: password
    });

    user.save(function(err) {
      if (err) { return next(err); }

      // Respond to request indicating the user was created
      res.json({ token: tokenForUser(user) });
    });
  });
}
