const Speaker = require('../models/speaker');
const User = require('../models/user');

exports.speakerExistsHandler = function (req,res){
  const speakerID = parseInt(req.body.speakerID);
  const languageID = parseInt(req.body.speakerLangID);
  const userID = req.body.userID;
  const slectedLangID = parseInt(req.body.languageID);
 // See if a Speaker with the given userID exists
   if (!(speakerID)){
     res.status(400).send({error:"this was a bad request"});
   }
  Speaker.findOne({ speakerID: speakerID, languageID:languageID, userID:userID }, function(err, existingSpeaker) {
   if (err) {
     return res.json(err)
   }
   // If a speaker with speakerID does exist, return an error
   if (existingSpeaker) {
     var out={}
     out.exists = true;
     out.data = existingSpeaker;
     out.slectedLangID = slectedLangID;
     return res.json(out);
   }else{
     return res.json({exists:false })
   }
 })
}

exports.speakerAddHandler = function (req,res){
  const speakerID = parseInt(req.body.speakerID);
  const name = req.body.name;
  const age = parseInt(req.body.age);
  const gender = req.body.gender;
  const contact = req.body.contact;
  const dialect = req.body.dialect;
  const languageID = parseInt(req.body.languageID);
  const userID = req.body.userID;
  // find a user if true.
  User.findOne({userID:userID},(err, existingUser)=>{
    if(!existingUser){
        res.status(400).send({error:"this was a bad request"});
    }else{
      console.log(userID);
      // create and save speaker record
      const speaker = new Speaker({
           speakerID: speakerID,
           name:name,
           age: parseInt(age),
           gender:gender,
           contact:contact,
           dialect:dialect,
           languageID:parseInt(languageID),
           userID:userID
       });
       speaker.save(function(err,newSpeaker) {
         if (err) { return next(err); }
         // Respond to request indicating the user was created
         res.json(newSpeaker);
       });
    }
  })
}
