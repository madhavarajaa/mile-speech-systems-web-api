const Utterances = require('../models/utterance');

exports.getUtterances = function (req,res){
  const speakerID = parseInt(req.body.speakerID);
  const languageID = parseInt(req.body.languageID);
  const userID = req.body.userID;
  console.log({ speakerID: speakerID, languageID:languageID, userID:userID });
  Utterances.find({ speakerID: speakerID, langID:languageID, userID:userID }, function(err, utterList) {
   if (err) {
     return res.json({error:err})
   }
   // If a speaker with speakerID does exist, return an error
   console.log(utterList);
   if (utterList) {
     return res.json(utterList);
   }else{
     return res.json([])
   }
 })
}

exports.postUtterances = function (req,res){
  res.send('post the utteranaces');
}
