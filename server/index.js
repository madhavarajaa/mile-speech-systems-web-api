const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const mporgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const router = require('./router');

const seedData = require('./seed/index');
// just calling all the models.

app.use(express.static(__dirname + '/build'));

// db setup
const local = 'mongodb://127.0.0.1:27017/milespeechsystems';
const mlab ='mongodb://admin:theadminpassword1234@ds123963.mlab.com:23963/milespeechsystems'
mongoose.connect(local, {
    useMongoClient: true,
});

// app setup
app.use(mporgan('dev'));
app.use(cors());
app.use(bodyParser.json({type: '*/*'}));
router(app);

if(process.argv.length>2){
  if(process.argv[2]=='--seed'){
    seedData();
  }
}
//seedData();
// server app
const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port);
console.log('Server listening on: 3090');
