const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// define our model
const langSchema = new Schema({
    name: String,
    langID: Number,
    utterPerSpeaker:Number
});


// create the model class
const model = mongoose.model('language', langSchema);

// export the model
module.exports = model;
