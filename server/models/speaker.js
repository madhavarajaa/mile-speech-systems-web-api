const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// define our model
const speakerSchema = new Schema({
    speakerID: Number,
    name:String,
    age:Number,
    gender:String,
    contact:String,
    dialect:String,
    languageID:Number,
    recordedUtterance:{ type: Number, default: 0 },
    recordedTime:{ type: Number, default: 0},
    userID:String
});


// create the model class
const model = mongoose.model('speaker', speakerSchema);

// export the model
module.exports = model;
