const mongoose = require('mongoose');



const Schema = mongoose.Schema;

// define our model
const utterSchema = new Schema({
    originalText:String,
    langID:Number,
    speakerID:Number,
    userID:String,
    isRecorded:{ type: Boolean, default:false },
    recText:{ type: String, default:'' },
    recAcessTime:Date,
    isQC1:{ type: Boolean, default:false },
    qc1Text:{ type: String, default:'' },
    qc1AccessTime:Date,
    isQC2:{ type: Boolean, default:false },
    qc2Text:{ type: String, default:'' },
    qc2AccessTime:Date,
    recTextErrFlag:{type:Boolean, default:true},
    originalTextErrFlag:{type:Boolean, default:true},
    qc1TextErrFlag:{type:Boolean, default:true},
    qc2TextErrFlag:{type:Boolean, default:true}
});


// create the model class
const model = mongoose.model('utterance', utterSchema);

// export the model
module.exports = model;
