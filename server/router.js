const Authentication   = require('./controllers/authentication');
const langController   = require('./controllers/languages');
const utterController  = require('./controllers/utterances');
const speakController  = require('./controllers/speaker');
const passwordService  = require('./services/passport');
const passport         = require('passport');

// seperating different auth strategies
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = (app) => {
    app.post('/api/signin', requireSignin, Authentication.signin);
    app.post('/api/signup', Authentication.signup);
    app.get('/api/getLanguages', langController.getLanguages);
    app.post('/api/speakerExists',speakController.speakerExistsHandler);
    app.post('/api/addSpeaker',speakController.speakerAddHandler);
    app.post('/api/getUtterances',utterController.getUtterances);
    // to finish
    app.post('/api/postUtterances',utterController.postUtterances);
    app.get('/api/', requireAuth, (req, res, next) => {
        res.send('This is api index page');
    });
};
