const Utterance = require('./utter.seed');
const Language = require('./lang.seed');
const User = require('./user.seed');


module.exports = async function SeedData(){
  await Utterance();
  console.log('Added all utterances');
  await User();
  console.log('Added all users');
  await Language();
  console.log('Added all Languages')
}
