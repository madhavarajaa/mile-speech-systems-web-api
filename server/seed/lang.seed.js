const Language = require('../models/language');
const AddEntry = require('../helpers/addEntryForSeed');
const csv=require('csvtojson')
const csvFilePath='./seed/data/language_table.csv'

var AddtoDB= async function(langSeedData){
  for(var i=0;i<langSeedData.length;i++){
    var d=langSeedData[i];
    var entry = new Language(d);
    await AddEntry(entry)
  }
}

module.exports = function getJSonFromCSV(){
  csv()
  .fromFile(csvFilePath)
  .then((jsonObj)=>{
    AddtoDB(jsonObj);
  })
}
