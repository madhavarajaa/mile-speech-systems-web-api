const Speaker = require('../models/speaker');
const AddEntry = require('../helpers/addEntryForSeed');

const speakerData=[
  {
      speakerID: 'engSpeaker',
      name:'Peter Parker',
      age:43,
      gender:'male',
      contact:'iam@spiderman.com',
      dialect:'New york',
      languageID:0,
      userID:'engUser'
  },{
      speakerID: 'hinSpeaker',
      name:'Ram',
      age:56,
      gender:'male',
      contact:'BatLight',
      dialect:'Delhi',
      languageID:1,
      userID:'hinUser'
  }
]


module.exports = async function(){
  for(var i=0;i<speakerData.length;i++){
    var d=speakerData[i];
    var entry = new Speaker(d);
    AddEntry(entry);
  }
}
