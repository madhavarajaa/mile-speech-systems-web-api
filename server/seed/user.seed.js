const User = require('../models/user');
const AddEntry = require('../helpers/addEntryForSeed');
const Language = require('../models/language');
const csv=require('csvtojson')
const csvFilePath='./seed/data/volunteer_table.csv'

var AddtoDB= module.exports = async function(userData){
  for(var i=0;i<userData.length;i++){
    var d=userData[i];
    var entry = new User(d);
    await AddEntry(entry);
  }
}

module.exports = function getJSonFromCSV(){
  csv()
  .fromFile(csvFilePath)
  .then((jsonObj)=>{
    AddtoDB(jsonObj);
  })
}
