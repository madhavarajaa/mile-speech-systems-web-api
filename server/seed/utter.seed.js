const Utterance = require('../models/utterance');
const AddEntry = require('../helpers/addEntryForSeed');
var readTextFile = require('read-text-file');

const LangIDFilePath='./seed/data/utter/lang_id.txt'
const origTextFilePath='./seed/data/utter/orig_text.txt'
const spkIDFilePath='./seed/data/utter/spk_id.txt'
const userIDFilePath='./seed/data/utter/volunteer_id.txt'

var langIDString = readTextFile.readSync(LangIDFilePath);
var origTextString = readTextFile.readSync(origTextFilePath);
var speakerIDString = readTextFile.readSync(spkIDFilePath);
var userIDString = readTextFile.readSync(userIDFilePath);

const langIDArray = langIDString.split('\n');
const origTextArray = origTextString.split('\n');
const speakerIDArray = speakerIDString.split('\n');
const userIDArray = userIDString.split('\n');

var utterData = [];
for(i=0;i<langIDArray.length;i++){
  var temp = {}
  temp.langID = parseInt(langIDArray[i]);
  temp.speakerID = parseInt(speakerIDArray[i]);
  temp.originalText = origTextArray[i];
  temp.userID = userIDArray[i];
  utterData.push(temp);
}

module.exports = async function(){
  for(var i=0;i<utterData.length;i++){
    var d=utterData[i];
    var entry = new Utterance(d);
    AddEntry(entry);
  }
}
